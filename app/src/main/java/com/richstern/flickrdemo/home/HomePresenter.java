package com.richstern.flickrdemo.home;

import com.richstern.flickrdemo.app.Injector;
import com.richstern.flickrdemo.models.Photos;
import com.richstern.flickrdemo.models.PhotosResponse;
import com.richstern.flickrdemo.net.FlickrService;

import javax.inject.Inject;

import io.reactivex.Observable;

public class HomePresenter {

    private static final String DEFAULT_EXTRAS = "url_q,url_z,date_upload,date_taken,owner_name,geo,tags";

    @Inject
    FlickrService flickrService;

    HomePresenter() {
        Injector.get().inject(this);
    }

    Observable<Photos> getRecentPhotos() {
        return flickrService
            .getRecentPhotos(DEFAULT_EXTRAS)
            .map(PhotosResponse::getPhotos);
    }

    Observable<Photos> getPopularPhotos() {
        return flickrService
            .getPopularPhotos(DEFAULT_EXTRAS)
            .map(PhotosResponse::getPhotos);
    }

    Observable<Photos> search(String tags) {
        return flickrService
            .search(tags, DEFAULT_EXTRAS, "interestingness-desc")
            .map(PhotosResponse::getPhotos);
    }
}
