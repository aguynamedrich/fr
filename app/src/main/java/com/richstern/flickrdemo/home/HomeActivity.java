package com.richstern.flickrdemo.home;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.richstern.flickrdemo.R;
import com.richstern.flickrdemo.models.Photo;
import com.richstern.flickrdemo.models.Photos;
import com.richstern.flickrdemo.photo.PhotoActivity;
import com.richstern.flickrdemo.util.Lists;
import com.richstern.flickrdemo.util.Objects;
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class HomeActivity extends RxAppCompatActivity {

    private HomePresenter presenter;
    private RecentPhotosAdapter adapter;

    @BindView(R.id.photos) RecyclerView photos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        adapter = new RecentPhotosAdapter();
        photos.setLayoutManager(new GridLayoutManager(this, 3));
        photos.setAdapter(adapter);

        presenter = new HomePresenter();
        presenter.search("sunset,beach,sky")
            .compose(bindToLifecycle())
            .filter(Objects::notNull)
            .map(Photos::getPhotos)
            .filter(Lists::notEmpty)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                this::populateRecent,
                throwable -> {
                    Toast.makeText(this, "Error loading photos", Toast.LENGTH_SHORT).show();
                });

//        adapter.getPhotoSelected()
//            .compose(bindToLifecycle())
//            .subscribe(photo -> PhotoActivity.startActivity(this, photo));

        adapter.getPhotoSelectedTransition()
            .compose(bindToLifecycle())
            .subscribe(pair -> PhotoActivity.startActivity(this, pair));
    }

    private void populateRecent(List<Photo> photos) {
        adapter.setPhotos(photos);
    }
}
