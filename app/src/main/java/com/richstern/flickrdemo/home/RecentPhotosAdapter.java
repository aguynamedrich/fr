package com.richstern.flickrdemo.home;

import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jakewharton.rxbinding2.view.RxView;
import com.jakewharton.rxrelay2.PublishRelay;
import com.richstern.flickrdemo.R;
import com.richstern.flickrdemo.models.Photo;
import com.richstern.flickrdemo.util.ViewResizeHelper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;

import static com.richstern.flickrdemo.util.ViewResizeHelper.AspectRatio.SQUARE;
import static com.richstern.flickrdemo.util.ViewResizeHelper.ResizeAnchor.WIDTH;

public class RecentPhotosAdapter extends RecyclerView.Adapter<RecentPhotosAdapter.RecentPhotoViewHolder> {

    private final List<Photo> photos = new ArrayList<>();

    private final PublishRelay<Photo> photoSelectedRelay = PublishRelay.create();
    private final PublishRelay<Pair<View, Photo>> photoSelectedTransitionRelay = PublishRelay.create();

    public void setPhotos(List<Photo> photos) {
        this.photos.clear();
        this.photos.addAll(photos);
        notifyDataSetChanged();
    }

    @Override
    public RecentPhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.list_item_recent_photo, parent, false);
        return new RecentPhotoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecentPhotoViewHolder holder, int position) {
        Photo photo = photos.get(position);

        Picasso.get()
            .load(photo.getUrlLargeSquare())
            .fit().centerCrop()
            .into(holder.thumbnail);

        RxView.clicks(holder.itemView)
            .throttleFirst(250, TimeUnit.MILLISECONDS)
            .map(__ -> Pair.create(holder.itemView, photo))
            .subscribe(pair -> {
                photoSelectedRelay.accept(pair.second);
                photoSelectedTransitionRelay.accept(pair);
            });
    }

    public Observable<Photo> getPhotoSelected() {
        return photoSelectedRelay;
    }

    public Observable<Pair<View, Photo>> getPhotoSelectedTransition() {
        return photoSelectedTransitionRelay;
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    class RecentPhotoViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.recent_photo_thumbnail) ImageView thumbnail;

        RecentPhotoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            ViewResizeHelper.resize(thumbnail, SQUARE, WIDTH);
        }
    }
}
