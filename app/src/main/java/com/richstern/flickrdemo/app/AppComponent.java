package com.richstern.flickrdemo.app;

import com.richstern.flickrdemo.home.HomePresenter;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = { AppModule.class })
public interface AppComponent {
    void inject(HomePresenter homePresenter);
}
