package com.richstern.flickrdemo.app;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.richstern.flickrdemo.net.FlickrService;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.schedulers.Schedulers;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class AppModule {

    private final Flickr appContext;

    public AppModule(Flickr appContext) {
        this.appContext = appContext;
    }

    @Provides
    Flickr getAppContext() {
        return appContext;
    }


    @Singleton
    @Provides
    Gson provideGson() {
        return new GsonBuilder().create();
    }

    @Singleton
    @Provides
    OkHttpClient provideHttpClient(@Named("apiKeyInterceptor") Interceptor apiKeyInterceptor) {
        return new OkHttpClient.Builder()
            .addInterceptor(apiKeyInterceptor)
            .build();
    }

    @Singleton
    @Provides
    @Named("apiKeyInterceptor")
    Interceptor provideApiKeyInterceptor() {
        return chain -> {
            Request request = chain.request();
            HttpUrl url = request
                .url()
                .newBuilder()
                .addQueryParameter("api_key", FlickrService.API_KEY)
                .addQueryParameter("format", "json")
                .addQueryParameter("nojsoncallback", "1")
                .build();
            request = request.newBuilder().url(url).build();
            return chain.proceed(request);
        };
    }

    @Provides
    @Singleton
    FlickrService provideFlickrService(OkHttpClient client, Gson gson) {
        RxJava2CallAdapterFactory rxAdapter = RxJava2CallAdapterFactory
            .createWithScheduler(Schedulers.io());

        Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(FlickrService.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(client)
            .addCallAdapterFactory(rxAdapter)
            .build();
        return retrofit.create(FlickrService.class);
    }
}
