package com.richstern.flickrdemo.app;

public class Injector {

    public static AppComponent get() {
        return Flickr.get().getComponent();
    }
}
