package com.richstern.flickrdemo.app;

import android.app.Application;

public class Flickr extends Application {

    private static Flickr INSTANCE = null;
    private AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        INSTANCE = this;

        component = DaggerAppComponent.builder()
            .appModule(new AppModule(this))
            .build();
    }

    public static Flickr get() {
        return INSTANCE;
    }

    public AppComponent getComponent() {
        return component;
    }
}
