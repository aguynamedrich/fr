package com.richstern.flickrdemo.photo;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.github.chrisbanes.photoview.PhotoView;
import com.richstern.flickrdemo.R;
import com.richstern.flickrdemo.models.Photo;
import com.squareup.picasso.Picasso;

import java.util.Optional;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PhotoActivity extends AppCompatActivity {

    private static final String EXTRA_PHOTO = "PhotoActivity.Photo";

    @BindView(R.id.image) PhotoView image;
    @BindView(R.id.owner) TextView owner;
    @BindView(R.id.date) TextView date;
    @BindView(R.id.title) TextView title;

    private Photo photo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        ButterKnife.bind(this);

        Optional.ofNullable(getIntent().getExtras().getSerializable(EXTRA_PHOTO))
            .filter(serializable -> Photo.class.isAssignableFrom(serializable.getClass()))
            .ifPresent(serializable -> {
                photo = (Photo) serializable;
                Picasso.get().load(photo.getUrlMedium()).fit().centerCrop().into(image);
                owner.setText(photo.getOwnerName());
                title.setText(photo.getTitle());
                date.setText(photo.getDateTaken());
            });
    }

    public static void startActivity(Context context, Photo photo) {
        Intent intent = new Intent(context, PhotoActivity.class);
        intent.putExtra(EXTRA_PHOTO, photo);
        context.startActivity(intent);
    }

    public static void startActivity(Activity activity, Pair<View, Photo> pair) {
        Intent intent = new Intent(activity, PhotoActivity.class);
        View view = pair.first;
        Photo photo = pair.second;
        intent.putExtra(EXTRA_PHOTO, photo);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
            activity, view, "photo");
        activity.startActivity(intent, options.toBundle());
    }
}
