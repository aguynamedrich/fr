package com.richstern.flickrdemo.net;

import com.richstern.flickrdemo.models.PhotosResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface FlickrService {

    String API_KEY = "f66d475dadcbd22c18c37753b6fe8876";
    String SECRET = "57d434ba7202a35b";
    String BASE_URL = " https://api.flickr.com/services/rest/";

    @GET("?method=flickr.photos.getRecent")
    Observable<PhotosResponse> getRecentPhotos(@Query("extras") String extras);

    @GET("?method=flickr.photos.getPopular")
    Observable<PhotosResponse> getPopularPhotos(@Query("extras") String extras);

    @GET("?method=flickr.photos.search")
    Observable<PhotosResponse> search(
        @Query("tags") String tags,
        @Query("extras") String extras,
        @Query("sort") String sort);
}
