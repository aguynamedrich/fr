package com.richstern.flickrdemo.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Photo implements Serializable {

    private String id;
    private String owner;
    private String secret;
    private String title;

    @SerializedName("datetaken")
    private String dateTaken;

    @SerializedName("ownername")
    private String ownerName;

    @SerializedName("url_q")
    private String imageUrlLargeSquare;

    @SerializedName("url_z")
    private String imageUrlMedium;

    @SerializedName("ispublic")
    private String isPublic;

    public String getUrlLargeSquare() {
        return imageUrlLargeSquare;
    }

    public String getUrlMedium() {
        return imageUrlMedium;
    }

    public String getTitle() {
        return title;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public String getDateTaken() {
        return dateTaken;
    }
}
