package com.richstern.flickrdemo.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Photos {

    public static final Photos EMPTY = new Photos();

    @SerializedName("photo")
    private List<Photo> photos = new ArrayList<>();

    public List<Photo> getPhotos() {
        return photos;
    }
}
